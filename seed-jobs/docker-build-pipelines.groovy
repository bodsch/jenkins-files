#!/usr/bin/env groovy

ArrayList pipelines = [
        [
                "pipeline_name": "CONTAINER-molecule-archlinux",
                "description"  : "molecule container - archlinux",
                "distribution" : "archlinux",
                "groovy_file"  : "molecule-jenkinsfile.groovy"
        ],
        [
                "pipeline_name": "CONTAINER-molecule-artixlinux",
                "description"  : "molecule container - artixlinux",
                "distribution" : "artixlinux",
                "groovy_file"  : "molecule-jenkinsfile.groovy"
        ],
        [
                "pipeline_name": "CONTAINER-molecule-debian-10",
                "description"  : "molecule container - debian 10",
                "distribution" : "debian",
                "version"      : "10",
                "groovy_file"  : "molecule-jenkinsfile.groovy"
        ],
        [
                "pipeline_name": "CONTAINER-molecule-debian-11",
                "description"  : "molecule container - debian 11",
                "distribution" : "debian",
                "version"      : "11",
                "groovy_file"  : "molecule-jenkinsfile.groovy"
        ],
        [
                "pipeline_name": "CONTAINER-molecule-debian-12",
                "description"  : "molecule container - debian 12",
                "distribution" : "debian",
                "version"      : "12",
                "groovy_file"  : "molecule-jenkinsfile.groovy"
        ]
]

pipelines.each { pipeline ->
    String pipeline_name = pipeline["pipeline_name"].trim()
    String pipeline_description = pipeline["description"].trim()
    String branch_name = (pipeline["branch_name"] ?: "main").trim()
    String groovy_file = (pipeline["groovy_file"] ?: "").trim()

    pipelineJob(pipeline_name) {
        displayName(pipeline_description)
        description(pipeline_description)

        properties {
            pipelineTriggers {
                triggers {
                    cron {
                        spec("0 4 * * 0")
                    }
                    pollSCM {
                        scmpoll_spec('')
                    }
                }
            }
        }

        // logRotator(int daysToKeep = -1, int numToKeep = -1, int artifactDaysToKeep = -1, int artifactNumToKeep = -1)
        logRotator {
            numToKeep(3)
        }

        environmentVariables {
            env('DISTRIBUTION', (pipeline["distribution"] ?: "").trim())
            env('VERSION', (pipeline["version"] ?: "latest").trim())
        }

        definition {
            cpsScm {
                scm {
                    git {
                        remote {
                            url("git@gitlab.com:bodsch/jenkins-files.git")
                            credentials("bodsch.gitlab")
                            branch("*/${branch_name}")
                        }
                    }
                }
                scriptPath("jenkins-files/docker/build/${groovy_file}")
                lightweight(false)
            }
        }
    }
}


listView('Container') {
    jobs {
        regex('^CONTAINER.*\$')
    }
    columns {
        buildButton()
        status()
        weather()
        name()
        lastSuccess()
        lastFailure()
        lastDuration()
    }
}
