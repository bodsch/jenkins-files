#!/usr/bin/env groovy

ArrayList pipelines = [
        [
                "pipeline_name": "ANSIBLE-prepare",
                "description"  : "prepare ansible environment",
                "groovy_file"  : "ansible-prepare.groovy"
        ]
]

pipelines.each { pipeline ->
    String pipeline_name = pipeline["pipeline_name"].trim()
    String pipeline_description = pipeline["description"].trim()
    String branch_name = (pipeline["branch_name"] ?: "main").trim()
    String groovy_file = (pipeline["groovy_file"] ?: "").trim()

    pipelineJob(pipeline_name) {
        displayName(pipeline_description)
        description(pipeline_description)

        // logRotator(int daysToKeep = -1, int numToKeep = -1, int artifactDaysToKeep = -1, int artifactNumToKeep = -1)
        logRotator {
            numToKeep(5)
        }

        definition {
            cpsScm {
                scm {
                    git {
                        remote {
                            url("git@gitlab.com:bodsch/jenkins-files.git")
                            credentials("bodsch.gitlab")
                            branch('*/' + "${branch_name}")
                        }
                    }
                }
                scriptPath("jenkins-files/ansible/" + "${groovy_file}")
                lightweight(false)
            }
        }
    }
}
