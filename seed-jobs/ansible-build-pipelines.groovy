#!/usr/bin/env groovy

ArrayList pipelines = [
        [
                "pipeline_name": "ANSIBLE-ROLE-algernon",
                "description"  : "ansible role: algernon",
                "ansible_role" : "ansible-algernon"
        ],
        [
                "pipeline_name": "ANSIBLE-ROLE-awscli",
                "description"  : "ansible role: awscli",
                "ansible_role" : "ansible-awscli"
        ],
        [
                "pipeline_name": "ANSIBLE-ROLE-glauth",
                "description"  : "ansible role: glauth",
                "ansible_role" : "ansible-glauth"
        ],
        [
                "pipeline_name": "ANSIBLE-ROLE-influxdb",
                "description"  : "ansible role: influxdb",
                "ansible_role" : "ansible-influxdb"
        ],
        [
                "pipeline_name": "ANSIBLE-ROLE-dovecot",
                "description"  : "ansible role: dovecot",
                "ansible_role" : "ansible-dovecot"
        ],
        [
                "pipeline_name": "ANSIBLE-COLLECTION-docker",
                "description"  : "ansible collection: docker",
                "ansible_collection" : "ansible-collection-docker",
                "groovy_file"  : "ansible-collection.groovy"
        ],
]

pipelines.each { pipeline ->
    String pipeline_name = pipeline["pipeline_name"].trim()
    String pipeline_description = pipeline["description"].trim()
    String branch_name = (pipeline["branch_name"] ?: "main").trim()
    String ansible_role = (pipeline["ansible_role"] ?: "").trim()
    String ansible_collection = (pipeline["ansible_collection"] ?: "").trim()
    String groovy_file = (pipeline["groovy_file"] ?: "ansible-role.groovy").trim()

    pipelineJob(pipeline_name) {
        displayName(pipeline_description)
        description(pipeline_description)

        properties {
            pipelineTriggers {
                triggers {
                    cron {
                        spec("0 2 * * *")
                    }
                    pollSCM {
                        scmpoll_spec('')
                    }
                }
            }
        }

        // logRotator(int daysToKeep = -1, int numToKeep = -1, int artifactDaysToKeep = -1, int artifactNumToKeep = -1)
        logRotator {
            numToKeep(3)
        }

        environmentVariables {
            env('ANSIBLE_ROLE', ansible_role)
            env('ANSIBLE_COLLECTION', ansible_collection)
        }

        definition {
            cpsScm {
                scm {
                    git {
                        remote {
                            url("git@gitlab.com:bodsch/jenkins-files.git")
                            credentials("bodsch.gitlab")
                            branch("*/${branch_name}")
                        }
                    }
                }
                scriptPath("jenkins-files/ansible/${groovy_file}")
                lightweight(false)
            }
        }
    }
}


listView('Ansible Role') {
    jobs {
        regex('^ANSIBLE-ROLE-.*\$')
    }
    columns {
        buildButton()
        status()
        weather()
        name()
        lastSuccess()
        lastFailure()
        lastDuration()
    }
}
