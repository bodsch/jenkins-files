#!/usr/bin/env groovy

ArrayList pipelines = [
        [
                "pipeline_name": "SYS-delete-containers",
                "description"  : "delete old containers from local system",
        ]
]

pipelines.each { pipeline ->
    String pipeline_name = pipeline["pipeline_name"].trim()
    String pipeline_description = pipeline["description"].trim()
    String branch_name = (pipeline["branch_name"] ?: "main").trim()

    pipelineJob(pipeline_name) {
        displayName(pipeline_description)
        description(pipeline_description)

        // logRotator(int daysToKeep = -1, int numToKeep = -1, int artifactDaysToKeep = -1, int artifactNumToKeep = -1)
        logRotator {
            numToKeep(3)
        }

        definition {
            cpsScm {
                scm {
                    git {
                        remote {
                            url("git@gitlab.com:bodsch/jenkins-files.git")
                            credentials("bodsch.gitlab")
                            branch('*/' + "${branch_name}")
                        }
                    }
                }
                scriptPath('jenkins-files/docker/delete/jenkinsfile.groovy')
                lightweight(false)
            }
        }
    }
}
