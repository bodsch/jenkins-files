#!/usr/bin/env groovy

ArrayList pipelines = [
        [
                "pipeline_name"    : "ANSIBLE-DEPLOYMENT-sensors",
                "description"      : "ansible deployment: sensors",
                "ansible_inventory": "inventories/matrix/inventory.yml",
                "ansible_playbook" : "playbooks/matrix/sensors.yml"
        ]
]

pipelines.each { pipeline ->
    String pipeline_name = pipeline["pipeline_name"].trim()
    String pipeline_description = pipeline["description"].trim()
    String branch_name = (pipeline["branch_name"] ?: "main").trim()
    String ansible_inventory = (pipeline["ansible_inventory"] ?: "").trim()
    String ansible_playbook = (pipeline["ansible_playbook"] ?: "").trim()

    pipelineJob(pipeline_name) {
        displayName(pipeline_description)
        description(pipeline_description)
/*
        properties {
            pipelineTriggers {
                triggers { 
                    cron {
                        spec("0 2 * * *")
                    }
                    pollSCM { 
                        scmpoll_spec('') 
                    }
                }
            }
        }
*/
        // logRotator(int daysToKeep = -1, int numToKeep = -1, int artifactDaysToKeep = -1, int artifactNumToKeep = -1)
        logRotator {
            numToKeep(3)
        }

        environmentVariables {
            env('ANSIBLE_INVENTORY', ansible_inventory)
            env('ANSIBLE_PLAYBOOK', ansible_playbook)
        }

        definition {
            cpsScm {
                scm {
                    git {
                        remote {
                            url("git@gitlab.com:bodsch/jenkins-files.git")
                            credentials("bodsch.gitlab")
                            branch("*/${branch_name}")
                        }
                    }
                }
                scriptPath("jenkins-files/ansible/ansible-deployment.groovy")
                lightweight(false)
            }
        }
    }
}


listView('Ansible Deployments') {
    jobs {
        regex('^ANSIBLE-DEPLOYMENT-.*\$')
    }
    columns {
        buildButton()
        status()
        weather()
        name()
        lastSuccess()
        lastFailure()
        lastDuration()
    }
}
