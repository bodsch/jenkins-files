#!/usr/bin/env groovy

node() {

  def git_server = "https://github.com/bodsch"
  def repoitory = "ansible-influxdb"

  try {
    stage('cleanup') {
      deleteDir()
    }

    stage("git checkout & make") {
      sh """
        git clone ${git_server}/${repoitory}

        cd ${repoitory}

        export DOCKER_HOST=molecule.matrix.lan:2376

        make
        make verify
        make destroy
      """
    }
  }
  catch (err) {
    sh """
      cd ${repoitory}

      export DOCKER_HOST=molecule.matrix.lan:2376    
      
      make destroy
    """  
    currentBuild.result = 'FAILURE'
    throw err
  }
}
