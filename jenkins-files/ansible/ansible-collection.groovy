#!/usr/bin/env groovy

node() {

  def registry = "molecule.matrix.lan:2376"
  def git_server = "https://github.com/bodsch"

  currentBuild.result = 'SUCCESS'

  try {
        stage('cleanup') {
            deleteDir()
        }
        stage("git checkout") {
            sh """
            git clone ${git_server}/${ANSIBLE_COLLECTION}
          """
        }

        stage("make") {
          sh """
            set -x

            export DOCKER_HOST=${registry}

            cd ${ANSIBLE_COLLECTION}

            ANSIBLE_VERSION="6.7"
            DISTRIBUTION="debian12"
            COLLECTION_ROLE=
            COLLECTION_SCENARIO=default

            make \
              test \
                -e TOX_ANSIBLE="ansible_${ANSIBLE_VERSION}" \
                -e DISTRIBUTION="${DISTRIBUTION}" \
                -e COLLECTION_ROLE="${COLLECTION_ROLE}" \
                -e COLLECTION_SCENARIO="${COLLECTION_SCENARIO}"
          """
        }
  }
  catch (err) {
    sh """
      cd ${ANSIBLE_COLLECTION}

      export DOCKER_HOST=${registry}

      export ANSIBLE_VERSION="6.7"
      export DISTRIBUTION="debian12"
      export COLLECTION_ROLE=
      export COLLECTION_SCENARIO=default

      make destroy
    """
    currentBuild.result = 'FAILURE'
    throw err
  }
}
