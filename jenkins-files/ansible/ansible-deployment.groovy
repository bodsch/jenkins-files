#!/usr/bin/env groovy

node() {

  def git_server = "https://gitlab.com"

  currentBuild.result = 'SUCCESS'

  try {
        stage('cleanup') {
            deleteDir()
        }
        stage("git checkout") {
            sh """
            git clone \
              ${git_server}/matrix-home/ansible-roles-matrix.git \
              deployment
          """
        }

        stage("get galaxy requirements") {
          sh """
            set -x

            cd deployment

            ansible-galaxy install -r requirements.yml
          """
        }

        stage("") {
          sh """

            cd deployment

            # --vault-password-file vaults/matrix.lan_ansible-vault.pass \
            export VAULT_PASSWORD_FILES=""

            ANSIBLE_OPTS="--inventory ${ANSIBLE_INVENTORY} "

            ansible-playbook \
               \${ANSIBLE_OPTS} \
              ${ANSIBLE_PLAYBOOK}

          """
        }
  }
  catch (err) {
    sh """
      cd ${ANSIBLE_ROLE}

      export DOCKER_HOST=molecule.matrix.lan:2376

      make destroy
    """
    currentBuild.result = 'FAILURE'
    throw err
  }
}
