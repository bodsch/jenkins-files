#!/usr/bin/env groovy

node() {

  def registry = "molecule.matrix.lan:2376"
  def git_server = "https://github.com/bodsch"

  currentBuild.result = 'SUCCESS'

  try {
        stage('cleanup') {
            deleteDir()
        }
        stage("git checkout") {
            sh """
            git clone ${git_server}/${ANSIBLE_ROLE}
          """
        }

        stage("make") {
          sh """
            set -x

            export DOCKER_HOST=${registry}

            cd ${ANSIBLE_ROLE}

            make
            make verify
            make destroy
          """
        }
  }
  catch (err) {
    sh """
      cd ${ANSIBLE_ROLE}

      export DOCKER_HOST=${registry}

      make destroy
    """
    currentBuild.result = 'FAILURE'
    throw err
  }
}
