#!/usr/bin/env groovy

node() {

  def git_server = "https://github.com/bodsch"
  def repoitory = "ansible-loki"

  try {
    stage("git checkout & make") {
      sh """
        sudo apt update
        sudo apt install -y python3-pip

        sudo pip3 install \
          ansible
          ansible-lint
          tox \
          flake8
      """
    }
  }
  catch (err) {
    currentBuild.result = 'FAILURE'
    throw err
  }
}
