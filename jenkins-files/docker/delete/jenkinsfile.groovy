#!/usr/bin/env groovy

node() {
    def registry = 'unix:///run/docker.sock'

    currentBuild.result = 'SUCCESS'

    try {
        stage('cleanup') {
            deleteDir()
        }

        stage("deleting stopped containers") {
            sh """
                set +x
                export DOCKER_HOST=${registry}

                container=\$(docker ps -a -q)

                if [ -n "\$container" ]
                then
                  docker rm \$container
                fi
            """
        }

        stage("deleting untagged containers") {
            sh """
                set +x
                export DOCKER_HOST=${registry}

                container=\$(docker images -q -f dangling=true)
                if [ -n "\$container" ]
                then
                  docker rmi --force \$container
                fi
            """
        }

    }
    catch (err) {
        currentBuild.result = 'FAILURE'
        throw err
    }
}


/*
pipeline {
  agent any
  options {
    ansiColor('xterm')
    disableConcurrentBuilds()
  }

  stages {

    stage('Deleting stopped containers') {
      steps {
          sh """
            export DOCKER_HOST=\"unix:///run/docker.sock\"

            echo 'Deleting stopped containers'
            docker rm $(docker ps -a -q)
          """
      }
    }

    stage('Deleting untagged images') {
      steps {
        sh """
          export DOCKER_HOST=\"unix:///run/docker.sock\"

          echo 'Deleting untagged images'
          docker rmi --force $(docker images -q -f dangling=true)
        """
      }
    }
  }
}
*/
