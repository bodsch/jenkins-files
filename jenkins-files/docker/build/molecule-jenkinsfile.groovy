#!/usr/bin/env groovy

node() {
    def registry = 'unix:///run/docker.sock'
    def git_server = "https://github.com/bodsch"
    def repository = "docker-ansible"

    currentBuild.result = 'SUCCESS'

    try {
        stage('cleanup') {
            deleteDir()
        }
        stage("git checkout") {
            sh """
            git clone ${git_server}/${repository}
          """
        }

        stage("make") {
            sh """
            set -x
            export DOCKER_HOST=${registry}

            cd ${repository}

            if [ ${VERSION} ]
            then
              make -e DISTRIBUTION=${DISTRIBUTION} -e DISTRIBUTION_VERSION=${VERSION}
            else
              make -e DISTRIBUTION=${DISTRIBUTION} -e DISTRIBUTION_VERSION=latest
            fi
          """
        }
    }
    catch (err) {
        currentBuild.result = 'FAILURE'
        throw err
    }
}